# QuakeJS - Docker

QuakeJS is a port of [ioquake3](http://www.ioquake3.org) to JavaScript with the help of [Emscripten](http://github.com/kripken/emscripten).

To see a live demo, check out [http://www.quakejs.com](http://www.quakejs.com).


## Installation

First of all install docker and docker-compose:

```shell
sudo apt install docker
sudo apt install docker-compose
```

After that, in the folder where is this readme, execute the command:

```shell
sudo docker-compose up
```

The server will start with a default deathmatch

## Connecting to server

First, to connect to the default server created (you may replace 127.0.0.1 for the IP of your server):

[http://127.0.0.1:8080/play?connect%20127.0.0.1:27960&password%20valid_password&name%20your_name](http://127.0.0.1:8080/play?connect%20127.0.0.1:27960&password%20valid_password&name%20your_name)


## Available maps (this maps can be set in the files server.cfg. server_dm.cfg and server_ctf.cfg are good examples):

- Deathmatch:
q3dm1
q3dm7
q3dm9
q3dm17
q3tourney2

- Pro:
q3tourney4
q3dm6
q3dm13

- Capture the flag: 
q3wctf1
q3wctf2
q3wctf3


## Game types:

g_gametype: 
    1 = DM 
    2 = Tourney (1on1)
    3 = Team DM
    4 = CTF

