FROM ubuntu:18.04
MAINTAINER Ivan Falcao

RUN apt-get update -y \
    && apt-get install -y unzip zip opus-tools cmake git emscripten openjdk-8-jdk nodejs npm llvm \
    && rm -rf /var/lib/apt/lists/*


#RUN git clone --recurse-submodules https://github.com/inolen/quakejs /quakejs/
COPY ./quakejs /quakejs

WORKDIR /quakejs

RUN npm config set unsafe-perm true && \
    npm install --production && \
    echo '{ "content": "content.quakejs.com" }' > bin/web.json

COPY ./server_dm.cfg /quakejs/base/baseq3/server.cfg
#CMD node bin/web.js --config ./web.json

CMD node build/ioq3ded.js +set fs_game baseq3 +set dedicated 2 +exec server.cfg

# docker build -f ./Dockerfile -t falcao/quakejs:1.0 .
# docker run --rm -it --name quakejs -e SERVER=127.0.0.1 -e HTTP_PORT=8080 -p 8080:8080 -p 27960:27960 falcao/quakejs:1.0