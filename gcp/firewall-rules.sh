PROJECT_ID="ipf-tests"

gcloud config set project "$PROJECT_ID"

gcloud compute --project=$PROJECT_ID firewall-rules create quakejs-service-ingress --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:8080,tcp:27960 --source-ranges=0.0.0.0/0 --target-tags=quakejs